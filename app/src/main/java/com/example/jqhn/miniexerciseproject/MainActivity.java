package com.example.jqhn.miniexerciseproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showLayoutTwo(View v){
        setContentView(R.layout.screen_two);
    }

    public void showLayoutThree(View v){
        setContentView(R.layout.screen_three);
    }

    public void showLayoutFour(View v){
        setContentView(R.layout.screen_four);
    }

    public void backToHomeScreen(View v){
        setContentView(R.layout.activity_main);
    }

}
